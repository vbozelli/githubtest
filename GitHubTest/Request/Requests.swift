//
//  Requests.swift
//  GitHubTest
//
//  Created by Victor Bozelli Alvarez on 02/03/2018.
//  Copyright © 2018 Victor Bozelli Alvarez. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

final class Requests {
	
	//Method to get the most popular Java repositories from GitHub using the page parameter
	static func popularJavaRepositoriesWithPage(page: Int, completion: @escaping ((String?, [Repository]?) -> Void)) {
		requestData(urlPath: "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)", returnType: ResponseRepositories.self) { (error, response) in
			if let error = error {
				completion(error, nil)
			}
			else if let message = response!.message {
				completion(message, nil)
			}
			else {
				let repositories = response!.items!
				var errorOwner: String?
				let group = DispatchGroup()
				for repository in repositories {
					group.enter()
					requestUser(urlPath: repository.owner.url, completion: { (error, user) in
						if errorOwner == nil {
							if let error = error {
								errorOwner = error
							}
							else {
								repository.owner = user
							}
						}
						group.leave()
					})
				}
				group.notify(queue: DispatchQueue.main, execute: {
					completion(nil, repositories)
				})
			}
		}
	}
	
	//Method to get the pull requests of a repository using the page parameter
	static func pullRequestsForRepository(page: Int, repository: Repository, completion: @escaping ((String?, [PullRequest]?) -> Void)) {
		requestArrayData(urlPath: "https://api.github.com/repos/" + repository.owner.username + "/" + repository.name + "/pulls?page=\(page)", returnType: PullRequest.self) { (error, pullRequests) in
			if let error = error {
				completion(error, nil)
			}
			else {
				var errorUser: String?
				let group = DispatchGroup()
				for pullRequest in pullRequests! {
					group.enter()
					requestUser(urlPath: pullRequest.user.url, completion: { (error, user) in
						if errorUser == nil {
							if let error = error {
								errorUser = error
							}
							else {
								pullRequest.user = user
							}
						}
						group.leave()
					})
				}
				group.notify(queue: DispatchQueue.main, execute: {
					completion(nil, pullRequests)
				})
			}
		}
	}
	
	//Method to get the user using the url parameter from the user or owner json field
	fileprivate static func requestUser(urlPath: String, completion: @escaping ((String?, User?) -> Void)) {
		requestData(urlPath: urlPath, returnType: User.self) { (error, response) in
			if let error = error {
				completion(error, nil)
			}
			else if let message = response!.message {
				completion(message, nil)
			}
			else {
				completion(nil, response!)
			}
		}
	}
	
	fileprivate static func requestData<T: Mappable>(urlPath: String, returnType: T.Type, completion: @escaping ((String?, T?) -> Void)) {
		if NetworkReachabilityManager()!.isReachable {
			let url = URL(string: urlPath)!
			Alamofire.request(url).responseObject(completionHandler: { (response: DataResponse<T>) in
				if let error = response.error {
					completion(error.localizedDescription, nil)
				}
				else {
					completion(nil, response.result.value!)
				}
			})
		}
		else {
			completion("Not connected to the Internet", nil)
		}
	}
	
	fileprivate static func requestArrayData<T: Mappable>(urlPath: String, returnType: T.Type, completion: @escaping ((String?, [T]?) -> Void)) {
		if NetworkReachabilityManager()!.isReachable {
			let url = URL(string: urlPath)!
			Alamofire.request(url).responseArray(completionHandler: { (response: DataResponse<[T]>) in
				if let error = response.error {
					var json: Any!
					do {
						json = try JSONSerialization.jsonObject(with: response.data!)
					}
					catch {
					}
					if json is [String:Any] {
						let jsonDict = json as! [String:Any]
						if jsonDict.keys.contains("message") {
							completion(jsonDict["message"] as? String, nil)
						}
						else {
							completion(error.localizedDescription, nil)
						}
					}
					else {
						completion(error.localizedDescription, nil)
					}
				}
				else {
					completion(nil, response.result.value!)
				}
			})
		}
		else {
			completion("Not connected to the Internet", nil)
		}
	}
}
