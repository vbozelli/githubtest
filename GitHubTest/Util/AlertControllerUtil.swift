//
//  AlertControllerUtil.swift
//  GitHubTest
//
//  Created by Victor Bozelli Alvarez on 20/02/2018.
//  Copyright © 2018 Victor Bozelli Alvarez. All rights reserved.
//

import UIKit

final class AlertControllerUtil {
	static func showAlertController(title: String?, message: String?, actions: [UIAlertAction]? = nil, target: UIViewController)
	{
		let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
		if let actions = actions {
			for action in actions {
				alertController.addAction(action)
			}
		}
		else {
			alertController.addAction(UIAlertAction(title: "OK", style: .default))
		}
		target.present(alertController, animated: true, completion: nil)
	}
}
