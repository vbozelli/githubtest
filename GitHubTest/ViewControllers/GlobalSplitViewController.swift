//
//  GlobalSplitViewController.swift
//  GitHubTest
//
//  Created by Victor Bozelli Alvarez on 03/03/2018.
//  Copyright © 2018 Victor Bozelli Alvarez. All rights reserved.
//

import UIKit

final class GlobalSplitViewController: UISplitViewController, UISplitViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
		preferredDisplayMode = .allVisible
		self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
		return true
	}
}
