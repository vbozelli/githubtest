//
//  PullRequestsViewController.swift
//  GitHubTest
//
//  Created by Victor Bozelli Alvarez on 02/03/2018.
//  Copyright © 2018 Victor Bozelli Alvarez. All rights reserved.
//

import AlamofireImage
import MBProgressHUD
import UIKit

final class PullRequestsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
	@IBOutlet weak var pullRequestsTableView: UITableView!
	fileprivate lazy var loadingPullRequests = false
	fileprivate lazy var page = 1
	fileprivate var pullRequests = [PullRequest]()
	var repository: Repository?
	
    override func viewDidLoad() {
        super.viewDidLoad()
		pullRequestsTableView.tableFooterView = UIView()
		if let repository = repository {
			pullRequestsTableView.alpha = 0
			navigationItem.title = repository.name
			retrievePullRequests()
		}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return pullRequests.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let pullRequest = pullRequests[indexPath.row]
		let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestTableViewCell", for: indexPath) as! PullRequestTableViewCell
		cell.namePullRequestLabel.text = pullRequest.title
		cell.descriptionLabel.text = pullRequest.descriptionPull
		cell.nameLabel.text = pullRequest.user.name
		cell.usernameLabel.text = pullRequest.user.username
		cell.userImageView.image = nil
		cell.activityIndicatorView.startAnimating()
		cell.userImageView.af_setImage(withURL: URL(string: pullRequest.user.photoUrl)!, filter: AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: 40, height: 40), radius: 20)) { (reponse) in
			cell.activityIndicatorView.stopAnimating()
		}
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let pullRequest = pullRequests[indexPath.row]
		UIApplication.shared.openURL(URL(string: pullRequest.url)!)
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if !loadingPullRequests && scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height {
			loadingPullRequests = true
			page += 1
			retrievePullRequests()
		}
	}
	
	func setRepository(repository: Repository) {
		self.repository = repository
		pullRequestsTableView.alpha = 0
		navigationItem.title = repository.name
		pullRequests.removeAll(keepingCapacity: true)
		retrievePullRequests()
	}
	
	fileprivate func retrievePullRequests() {
		
		let progressHud = MBProgressHUD.showAdded(to: view, animated: true)
		progressHud.detailsLabel.text = "Loading pull request for this repository. Please wait..."
		Requests.pullRequestsForRepository(page: page, repository: repository!) { (error, pullRequests) in
			progressHud.hide(animated: true)
			if let error = error {
				AlertControllerUtil.showAlertController(title: "Warning", message: error, target: self)
			}
			else {
				self.pullRequests.append(contentsOf: pullRequests!)
				self.pullRequestsTableView.reloadData()
				if self.pullRequestsTableView.alpha == 0 {
					UIView.animate(withDuration: 0.25, animations: {
						self.pullRequestsTableView.alpha = 1
					})
				}
				if self.loadingPullRequests {
					self.loadingPullRequests = false
					var indexPath = self.pullRequestsTableView.indexPathsForVisibleRows!.last!
					indexPath.row += 1
					self.pullRequestsTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
				}
			}
		}
	}
}
