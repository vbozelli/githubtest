//
//  RepositoriesViewController.swift
//  GitHubTest
//
//  Created by Victor Bozelli Alvarez on 01/03/2018.
//  Copyright © 2018 Victor Bozelli Alvarez. All rights reserved.
//

import AlamofireImage
import MBProgressHUD
import UIKit

final class RepositoriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	@IBOutlet weak var repositoriesTableView: UITableView!
	fileprivate lazy var firstTime = true
	fileprivate lazy var loadingRepositories = false
	fileprivate lazy var page = 1
	fileprivate lazy var repositories = [Repository]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		repositoriesTableView.alpha = 0
		repositoriesTableView.tableFooterView = UIView()
		retrieveRepositories()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return repositories.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let repository = repositories[indexPath.row]
		let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryTableViewCell", for: indexPath) as! RepositoryTableViewCell
		cell.descriptionLabel.text = repository.description
		cell.repositoryNameLabel.text = repository.name
		cell.forksLabel.text = String(repository.forks)
		cell.starsLabel.text = String(repository.stars)
		let owner = repository.owner!
		cell.nameLabel.text = owner.name
		cell.usernameLabel.text = owner.username
		cell.userImageView.image = nil
		cell.activityIndicatorView.startAnimating()
		cell.userImageView.af_setImage(withURL: URL(string: owner.photoUrl)!, filter: AspectScaledToFillSizeWithRoundedCornersFilter(size: CGSize(width: 40, height: 40), radius: 20), completion: { (response) in
			cell.activityIndicatorView.stopAnimating()
		})
		return cell
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if !loadingRepositories && scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height {
			loadingRepositories = true
			page += 1
			retrieveRepositories()
		}
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let repository = repositories[indexPath.row]
		if UIDevice.current.userInterfaceIdiom == .phone {
			let pullRequestsViewController = storyboard!.instantiateViewController(withIdentifier: "PullRequestsViewController") as! PullRequestsViewController
			pullRequestsViewController.repository = repository
			navigationController!.pushViewController(pullRequestsViewController, animated: true)
		}
		else {
			let pullRequestsViewController = (self.splitViewController!.viewControllers[1] as! UINavigationController).viewControllers[0] as! PullRequestsViewController
			pullRequestsViewController.setRepository(repository: repository)
		}
	}
	
	fileprivate func retrieveRepositories() {
		let progressHud = MBProgressHUD.showAdded(to: view, animated: true)
		progressHud.detailsLabel.text = "Loading popular GitHub Java repositories. Please wait..."
		Requests.popularJavaRepositoriesWithPage(page: page) { (error, repositories) in
			progressHud.hide(animated: true)
			if let error = error {
				AlertControllerUtil.showAlertController(title: "Warning", message: error, target: self)
			}
			else {
				self.repositories.append(contentsOf: repositories!)
				self.repositoriesTableView.reloadData()
				if self.firstTime {
					self.firstTime = false
					UIView.animate(withDuration: 0.25, animations: {
						self.repositoriesTableView.alpha = 1
					})
					if UIDevice.current.userInterfaceIdiom == .pad {
						let viewControllers = self.splitViewController!.viewControllers
						if viewControllers.count > 1 {
							let pullRequestsViewController = (viewControllers[1] as! UINavigationController).viewControllers[0] as! PullRequestsViewController
							pullRequestsViewController.setRepository(repository: repositories![0])
						}
					}
				}
				if self.loadingRepositories {
					self.loadingRepositories = false
					var indexPath = self.repositoriesTableView.indexPathsForVisibleRows!.last!
					indexPath.row += 1
					self.repositoriesTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
				}
			}
		}
	}
}
