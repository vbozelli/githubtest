//
//  PullRequestTableViewCell.swift
//  GitHubTest
//
//  Created by Victor Bozelli Alvarez on 03/03/2018.
//  Copyright © 2018 Victor Bozelli Alvarez. All rights reserved.
//

import UIKit

final class PullRequestTableViewCell: UITableViewCell {
	@IBOutlet weak var namePullRequestLabel: UILabel!
	@IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var usernameLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var userImageView: UIImageView!
}
