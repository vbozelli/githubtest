//
//  RepositoryTableViewCell.swift
//  GitHubTest
//
//  Created by Victor Bozelli Alvarez on 02/03/2018.
//  Copyright © 2018 Victor Bozelli Alvarez. All rights reserved.
//

import UIKit

final class RepositoryTableViewCell: UITableViewCell {
	@IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
	@IBOutlet weak var usernameLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var repositoryNameLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var starsLabel: UILabel!
	@IBOutlet weak var forksLabel: UILabel!
	@IBOutlet weak var userImageView: UIImageView!
}
