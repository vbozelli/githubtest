//
//  ResponseRepositories.swift
//  GitHubTest
//
//  Created by Victor Bozelli Alvarez on 02/03/2018.
//  Copyright © 2018 Victor Bozelli Alvarez. All rights reserved.
//

import ObjectMapper

final class ResponseRepositories: Mappable {
	var message: String?
	var items: [Repository]!
	
	init?(map: Map) {
	}
	
	func mapping(map: Map) {
		message <- map["message"]
		items <- map["items"]
	}
}
