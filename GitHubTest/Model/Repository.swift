//
//  Repository.swift
//  GitHubTest
//
//  Created by Victor Bozelli Alvarez on 02/03/2018.
//  Copyright © 2018 Victor Bozelli Alvarez. All rights reserved.
//

import ObjectMapper

final class Repository: Mappable {
	var forks: Int!
	var stars: Int!
	var description: String!
	var name: String!
	var owner: User!
	
	required init?(map: Map) {
	}
	
	func mapping(map: Map) {
		forks <- map["forks_count"]
		stars <- map["stargazers_count"]
		description <- map["description"]
		name <- map["name"]
		owner <- map["owner"]
	}
}
