//
//  Owner.swift
//  GitHubTest
//
//  Created by Victor Bozelli Alvarez on 02/03/2018.
//  Copyright © 2018 Victor Bozelli Alvarez. All rights reserved.
//

import ObjectMapper

final class User: Mappable {
	var message: String?
	var name: String?
	var username: String!
	var photoUrl: String!
	var url: String!
	
	init?(map: Map) {
	}
	
	func mapping(map: Map) {
		message <- map["message"]
		name <- map["name"]
		username <- map["login"]
		photoUrl <- map["avatar_url"]
		url <- map["url"]
	}
}
