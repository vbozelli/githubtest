//
//  PullRequest.swift
//  GitHubTest
//
//  Created by Victor Bozelli Alvarez on 03/03/2018.
//  Copyright © 2018 Victor Bozelli Alvarez. All rights reserved.
//

import ObjectMapper

final class PullRequest: Mappable {
	var title: String!
	var descriptionPull: String!
	var url: String!
	var user: User!
	
	init?(map: Map) {
	}
	
	func mapping(map: Map) {
		descriptionPull <- map["body"]
		title <- map["title"]
		url <- map["html_url"]
		user <- map["user"]
	}
}
